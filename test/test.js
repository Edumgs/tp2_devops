const chaiHttp = require('chai-http');
const chai = require('chai');
const expect = require('chai').expect;
const app = require('../app');

chai.use(chaiHttp);

describe('Test de autenticacion del usuario test', () => {
  it('Registro', done => {
    chai.request(app)
      .post('/signup')
      .send({email: 'test@mail.com', password: 'test1234'})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect('Location', '/profile');
        done();
      });
  }).timeout(0);

  it('Inicio de Sesion', done => {
      chai.request(app)
        .post('/login')
        .send({email: 'test@mail.com', password: 'test1234'})
        .end(function(err, res) {
          expect(res).to.have.status(200);
          expect('Location', '/profile');
          done();
        });
  }).timeout(0);
});

// chai.use(chaiHttp);
// chai.request('http://localhost:3000')
//   .post('/signup')
//   .type('form')
//   .send({email: 'test@mail.com', password: 'test1234'});

// describe('Inicio de sesion del usuario test', () => {
//   it('Inicio de Sesion', (done) => {
//     chai.request('http://localhost:3000')
//       .post('/login')
//       .type('form')
//       .send({email: 'test@mail.com', password: 'test1234'})
//       .end(function(err, res) {
//         expect(res).to.have.status(200);
//         expect('Location', '/profile');
//         done();
//       });
//   });
// });

