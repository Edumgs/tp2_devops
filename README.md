# Trabajo Practico
 - **Materia:** Gestión de Centro de Cómputos
 - **Profesora:** Ing. Ellen Mendez
 - **Alumno:** Eduardo Monges
 - **Tema:** DevOps

<div style="width: 600px;">
![DevOps](https://cdn-images-1.medium.com/max/2600/1*EBXc9eJ1YRFLtkNI_djaAw.png)
</div>


### Proyecto
Aplicación Web de Autenticación. El proyecto fue echo con [Node.js](https://nodejs.org/es/)

### Archivo .gitlab-ci.yml
Con este archivo se utiliza para la configuración de un repositorio para la integración continua del proyecto. Esto implica el building, testing y deploying cambios en el codigo continuamente, lo que reduce la posibilidad de desarrollar con versiones errores o fallidas
en cada paso.

```yml
image: node:11.15.0
# image -> Es para especificar una imagen de Docker, en este caso
# se usa node

stages:
# stages -> Es para definir etapas, en este caso existen dos etapas
# test: test unitario
# deploy: despliegue de los dos entornos
# Si una de las etapas falla se termina el proceso
  - test
  - deploy

Homologacion:
# En la etapa Homologacion (test): se especifica primero los servicios
# y variables necesario para la prueba unitaria
  stage: test
  services:
    - mongo:latest
  variables:
    MONGODB: 'mongodb://mongo/autenticacion'
  script:
    - npm i
    - npm test

Desarrollo:
# En la etapa Desarrollo (deploy): Se especifica una herramienta dpl
# que nos facilita el despliegue del ambiente de desarrollo utilizando
# las variables APP_NAME: es el nombre del app en heroku en este caso
# el staging y HEROKU_API_KEY: es una clave de acceso para el
# despliegue del proyecto
  image: ruby
  stage: deploy
  variables:
    APP_NAME: tpdevops-staging
    HEROKU_API_KEY: $HEROKU_API_KEY_STAGING
  before_script:
    - gem install dpl
  script:
    - dpl --provider=heroku --app=$APP_NAME --api-key=$HEROKU_API_KEY
  environment:
    name: tpdevops-staging
    url: https://tpdevops-staging.herokuapp.com/
  only:
    - master

Produccion:
# En la etapa Producion (deploy): Se especifica una herramienta dpl
# que nos facilita el despliegue del ambiente de produccion utilizando
# las variables APP_NAME: es el nombre del app en heroku en este caso
# el production y HEROKU_API_KEY: es una clave de acceso para el
# despliegue del proyecto
  image: ruby
  stage: deploy
  variables:
    APP_NAME: tpdevops-production
    HEROKU_API_KEY: $HEROKU_API_KEY_PRODUCTION
  before_script:
    - gem install dpl
  script:
    - dpl --provider=heroku --app=$APP_NAME --api-key=$HEROKU_API_KEY
  environment:
    name: tpdevops-production
    url: https://tpdevops-production.herokuapp.com/
  only:
    - master
```
